<?php
    use \Tamtamchik\SimpleFlash\Flash;
    class Users extends Controller{


        public function __construct() {
            // Desde aquí cargaremos los modelos.
            $this->userModel = $this->model('User');
        }

        
        public function register()
        {

            //Si recives un metodo POST de solicitud 
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                

                $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

                $data = [
                    'name' => trim($_POST['name']),
                    'email' =>  trim($_POST['email']),
                    'password' =>  trim($_POST['password']),
                    'confirm_password' => trim($_POST['confirm_password']),
                    'name_err' => '',
                    'email_err' => '',
                    'password_err' => '',
                    'confirm_password_err' => '',
                    'activa' => 'register'
                ];
                
                //Comprobante del nombre

                if(isset($_POST['name']) && empty($data['name'])){
                    $data['name_err'] = 'Campo vacio';
                }

                //Comprobante del confirm password
                
                if(isset($_POST['confirm_password']) && empty($data['confirm_password'])){
                    $data['confirm_password_err'] = 'Campo vacio';
                }elseif($data['password'] != $data['confirm_password']){
                    $data['confirm_password_err'] = 'No coinciden las contraseñas';
                }

                //Comprobante de la contraseña

                if(isset($_POST['password']) && empty($data['password'])){
                    $data['password_err'] = 'Campo vacio';
                }elseif(strlen( $data['password']) < 6){                                    
                    $data['password_err'] = 'La contraseña tiene que tener 6 carácteres';
                }

                //Comprobante del email

                if(isset($_POST['email']) && empty($data['email'])){
                    $data['email_err'] = 'Campo vacio';
                }elseif ($this->userModel->findUserByEmail($data['email'])){
                    $data['email_err'] = 'Email repetido';
                }
                
                if(empty($data['name_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])){
                    // echo "To-do: registrar usuario";

                    $data = [
                        'name' => trim($_POST['name']),
                        'email' =>  trim($_POST['email']),
                        'password' =>  trim(password_hash($_POST['password'], PASSWORD_DEFAULT)),
                        'confirm_password' => trim($_POST['confirm_password']),
                    ];

                    $this->userModel->register($data);

                    
                    $flash = new Flash();
                    $flash->message('Ya estás registrado y puedes iniciar sesión.', 'info');

                    //Llamar al helper
                    redirect('/users/login');

                }else{
                    $this->view('users/register', $data);
                }

            //Si no, rellena todos los value del array $data con un string vacio. 
            } else {

                $data = [
                    'name' => '',
                    'email' => '',
                    'password' => '',
                    'confirm_password' => '',
                    'name_err' => '',
                    'email_err' => '',
                    'password_err' => '',
                    'confirm_password_err' => '',
                    'activa' => 'register'
                ];
                
                //asigna la vista a users/register.
                $this->view('users/register', $data);
            }
        }

        public function login(){

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                $_POST = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

                $data = [
                    'email' =>  trim($_POST['email']),
                    'password' =>  trim($_POST['password']),
                    'email_err' => '',
                    'password_err' => '',
                    'activa' => 'login'
                ];
                

                //Comprobante de la contraseña

                if(isset($_POST['password']) && empty($data['password'])){
                    $data['password_err'] = 'Campo vacio';
                }elseif(strlen( $data['password']) < 6){                                    
                    $data['password_err'] = 'La contraseña tiene que tener 6 carácteres';
                }

                //Comprobante del email

                if(isset($_POST['email']) && empty($data['email'])){
                    $data['email_err'] = 'Campo vacio';
                }

                if(empty($data['email_err']) && empty($data['password_err'])){
                    //funciona


                    $user = $this->userModel->login($data['email'], $data['password']);

                    /*
                    var_dump($user);
                    echo"<br>";
                    echo $data['password'];
                    echo"<br>";
                    echo password_hash($_POST['password'], PASSWORD_DEFAULT);
                    echo"<br>";
                    echo hash('md5', $data['password']);
                    echo"<br>";
                    echo $data['email'];
                    */
                        if ($user) {
                            $this->createUserSession($user);
                        } else {
                            $data['password_err'] = 'Contraseña incorrecta';
                            $this->view('users/login', $data);
                        }
                }else{
                    $this->view('users/login', $data);
                }

                
                if ($this->userModel->findUserByEmail($data['email'])) {
                    // Lo haremos más adelante
                } else {
                    $data['email_err'] = 'El usuario no se ha encontrado';
                }

            
            //Si no, rellena todos los value del array $data con un string vacio. 
            } else {

                $data = [
                    'email' => '',
                    'password' => '',
                    'email_err' => '',
                    'password_err' => '',
                    'activa' => 'login'
                ];
                
                //asigna la vista a users/register.
                $this->view('users/login', $data);
            }
            
        }

        public function createUserSession($user){
            $_SESSION['id'] = $user->id ;
            $_SESSION['name'] = $user->name;
            $_SESSION['email'] = $user->email;
            
            redirect('/posts/index');
        }

        public function logout(){
            unset($_SESSION["id"]);
            unset($_SESSION["name"]);
            unset($_SESSION["email"]);
            
            session_destroy();
            redirect('/users/login');
        }

    }



?>
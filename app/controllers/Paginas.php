<?php

    class Paginas extends Controller
    {

        public function __construct() {
            // Desde aquí cargaremos los modelos.
        }

        public function index(){
            
            $data = [
                "activa" => 'home'
            ];

            $result = isLoggedh();
            if($result){
                redirect('/posts/index');
            }else{

                $this->view('paginas/index', $data);
            }
          
        }

        public function about(){           
            $data = [
                "activa" => 'about'
            ];
            return $this->view('paginas/about', $data);
        }

    }
    

?>
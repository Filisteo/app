<?php

use \Tamtamchik\SimpleFlash\Flash;

class Posts extends Controller
{


    public function __construct()
    {

        $result = isLoggedh();

        if (!$result) {
            redirect('/users/login');
        } else {
            $this->postModel = $this->model('Post');
            $this->userModel = $this->model('User');
        }
    }

    public function index()
    {

        $data = [
            'post' => $this->postModel->getPosts(),
            "activa" => ''
        ];

        $this->view('post/index', $data);
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'title' => trim($_POST['title']),
                'body' =>  trim($_POST['body']),
                'user_id' => $_SESSION['id'],
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'title_err' => '',
                'body_err' => '',
                'image_err' => '',
                "activa" => ''
            ];

            if (empty($data['title'])) {
                $data['title_err'] = 'Campo vacio';
            }

            if (empty($data['body'])) {
                $data['body_err'] = 'Campo vacio';
            }

            // PRUEBA
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /*
            if (!empty($data['image'])) {
                if ($_FILES['image']['error'] !== UPLOAD_ERR_OK) {
                    switch ($_FILES['image']['error']) {
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            $data['image_err'] = 'El fichero es demasiado grande. Tamaño máximo: ' . ini_get('upload_max_filesize');
                            break;
                        case UPLOAD_ERR_PARTIAL:
                            $data['image_err'] = "No se ha podido subir el fichero completo";
                            break;
                        default:
                            $data['image_err'] = "Error al subir el fichero";
                            break;
                    }
                } else {
                    $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                    if (in_array($_FILES['image']['type'], $arrTypes) === false) {
                        $data['image_err'] = "Tipo de fichero no soportado";
                    } else if (is_uploaded_file($_FILES['image']['tmp_name']) === false) {
                        $data['image_err'] = "El archivo no se ha subido mediante un formulario";
                    } else if (move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $_FILES['image']['name']) === false) {
                        $data['image_err'] = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
                    }
                }
            }*/



            
            if (!empty($data['image'])) {
                $arrType = ["image/jpeg", "image/png", "image/gif"];
                $file = new File($_FILES['image'], $arrType);
                var_dump($data['image']);
                try {
                    $file->prubeCase();
                    $file->saveUploadFile('img/');
                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (empty($data['title_err']) && empty($data['body_err']) && empty($data['image_err'])) {
                $this->postModel->addPOST($data);

                $flash = new Flash();
                $flash->message('Post guardado.', 'info');

                redirect('/posts/index');
            } else {
                $this->view('post/add', $data);
            }
        } else {
            $data = [
                'title' => '',
                'body' =>  '',
                'user_id' => '',
                'image' => '',
                'title_err' => '',
                'body_err' => '',
                'image_err' => '',
                "activa" => ''
            ];

            $this->view('post/add', $data);
        }
    }

    public function show($id)
    {
        $result = $this->postModel->getPostById($id);
        $result2 = $this->userModel->getUserById($_SESSION['id']);
        //var_dump($result);
        //echo '<br>';
        //var_dump($result2);

        $data = [
            'post' => $result,
            'user' => $result2,
            "activa" => ''
        ];

        $this->view('post/show', $data);
    }


    public function edit($id)
    {
        $post = $this->postModel->getPostById($id);
        $user = $this->userModel->getUserById($_SESSION['id']);


        $data = [
            'post' => $post,
            'user' => $user,
            "activa" => ''
        ];

        $this->view('post/edit', $data);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'id' => $id,
                'title' => trim($_POST['title']),
                'body' =>  trim($_POST['body']),
                'user_id' => $_SESSION['id'],
                'title_err' => '',
                'body_err' => '',
                "activa" => ''
            ];

            if (isset($_POST['title']) && empty($data['title'])) {
                $data['title_err'] = 'Campo vacio';
            }

            if (isset($_POST['body']) && empty($data['body'])) {
                $data['body_err'] = 'Campo vacio';
            }

            if (empty($data['title_err']) && empty($data['body_err'])) {

                $this->postModel->updatePost($data);

                $flash = new Flash();
                $flash->message('Post editado.', 'info');

                redirect('/posts/index');
            } else {
                $this->view('post/edit', $data);
            }
        } else {
            $data = [
                'title' => '',
                'body' =>  '',
                'user_id' => '',
                'title_err' => '',
                'body_err' => '',
                "activa" => ''
            ];

            $this->view('post/edit', $data);
        }
    }

    public function delete($id)
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post = $this->postModel->getPostById($id);
            $user = $this->userModel->getUserById($_SESSION['id']);

            if ($post->user_id == $user->id) {
                $this->postModel->deletePost($id);

                $flash = new Flash();
                $flash->message('Post borrado.', 'info');

                redirect('/posts/index');
            } else {
                redirect('/posts/index');
            }
        }
    }
}

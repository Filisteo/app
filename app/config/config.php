<?php

/*dirname:  Dada una cadena que contiene
la ruta a un fichero o directorio, esta función devolverá la ruta del directorio padre que está a niveles del directorio actual. 

Esta definiendo una ruta, desde el documentRoot, hasta el directorio que inmediatamente esta por encima del directorio que almacena config.php

*/

define('APPROOT', dirname(dirname(__FILE__)));
define('URLROOT','http://192.168.56.101/app');
define('SITENAME','MVCPosts');

define('APPVERSION','V1.0.0');

// Parámetros de la BBDD

define('DB_HOST', $_ENV['DB_HOST']);
define('DB_USER', $_ENV['DB_USER']);
define('DB_PASS', $_ENV['DB_PASS']);
define('DB_NAME', $_ENV['DB_NAME']);


?>


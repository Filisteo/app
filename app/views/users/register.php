<?php 
    require APPROOT . '/views/partials/header.php';
    require APPROOT . '/views/partials/navbar.php'; 
?>
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2> Crear una cuenta</h2>
            <p>Por favor llena los campos para poder registrarse</p>
            <form method="POST">     

                <div class="form-group has-validation">
                    <label for="name">Nombre: <sup>*</sup></label>
                    <input type="text" name="name" class="form-control <?= !empty($data['name_err'])? 'is-invalid' : ''?>" value="<?=$data['name']?>">
                    <div class="invalid-feedback"><?= $data['name_err'] ?></div> 
                </div>

                <div class="form-group has-validation">
                    <label for="name">Email: <sup>*</sup></label>
                    <input type="text" name="email" class="form-control <?= !empty($data['email_err'])? 'is-invalid' : ''?>" value="<?= empty($data['email_err'])? $data['email'] : ''?>">
                    <div class="invalid-feedback"><?= $data['email_err'] ?></div> 
                </div>

                <div class="form-group has-validation">
                    <label for="name">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?= !empty($data['password_err'])? 'is-invalid' : ''?>" value="<?=$data['password']?>">
                    <div class="invalid-feedback"><?= $data['password_err'] ?></div> 
                </div>

                <div class="form-group has-validation">
                    <label for="name">Confirma contraseña: <sup>*</sup></label>
                    <input type="password" name="confirm_password" class="form-control <?=!empty($data['confirm_password_err'])?'is-invalid' : ''?>" value="">
                    <div class="invalid-feedback"><?= $data['confirm_password_err'] ?></div> 
                </div>

                <div class="row">
                    <div class="col">
                        <a href="<?= URLROOT?>/users/login">¿Ya tienes cuenta? Inicia sesión</a>
                    </div>
                    <div class="col">
                        <input type="submit" value="Registrar" class="btn btn-danger btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    (function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()
</script>

<?php require APPROOT . '/views/partials/footer.php'; ?>


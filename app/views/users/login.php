<?php
    require APPROOT . '/views/partials/header.php';
    require APPROOT . '/views/partials/navbar.php'; 
?>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
               
                <h2>Iniciar sesión</h2>
                <p>Por favor, introduzca su correo y contraseña</p>
                <form method="POST">

                <div class="form-group has-validation">
                    <label for="name">Email: <sup>*</sup></label>
                    <input type="text" name="email" class="form-control <?= !empty($data['email_err'])? 'is-invalid' : ''?>" value="<?= empty($data['email_err'])? $data['email'] : ''?>">
                    <div class="invalid-feedback"><?= $data['email_err'] ?></div> 
                </div>
                
                <div class="form-group has-validation">
                    <label for="name">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?= !empty($data['password_err'])? 'is-invalid' : ''?>" value="<?=$data['password']?>">
                    <div class="invalid-feedback"><?= $data['password_err'] ?></div> 
                </div>

                    
                    <div class="flashes">
                    <?= (string) flash() ?>
                    </div>

                    <div class="row">
                        <div class="col">
                            <a href="<?= URLROOT?>/users/register">¿No tienes cuenta? Regístrate</a>
                        </div>
                        <div class="col">
                            <input type="submit" value="Iniciar sesión" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php require APPROOT . '/views/partials/footer.php'; ?>
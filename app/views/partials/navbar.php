<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">MVCPosts</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link <?= $data['activa'] == 'home' ? 'active' : '' ?>" aria-current="page" href="<?= URLROOT?>/paginas/index">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= $data['activa'] == 'about' ? 'active' : '' ?>" href="<?= URLROOT?>/paginas/about">About</a>
        </li>
        <?php
        $result = isLoggedh();

        if(!$result){          
          ?>
        <li class="nav-item">
          <a class="nav-link <?= $data['activa'] == 'register' ? 'active' : '' ?>" href="<?= URLROOT?>/users/register">Registrar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= $data['activa'] == 'login' ? 'active' : '' ?>" href="<?= URLROOT?>/users/login">Login</a>
        </li>
        <?php }else{?>
        <li class="nav-item">
          <a class="nav-link" href="<?= URLROOT?>/users/logout">Cerrar sesion</a>
        </li>
        <li>
          <p style="margin-top: 8px;"><?=$_SESSION['name'].' '.$_SESSION['email']?></p>
        </li>
        <?php }?>
      </ul>

    </div>
  </div>
</nav>


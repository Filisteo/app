<?php
    include_once APPROOT.'/views/partials/header.php';
    include_once APPROOT.'/views/partials/navbar.php';
?>

<div class="container mt-3">
  <h2>Bienvenido</h2>
  <div class="mt-4 p-5 bg-primary text-white rounded">
    <h1>Jumbotron Example</h1> 
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat..</p> 
  </div>
</div>

<?php
    include_once APPROOT.'/views/partials/footer.php';
?>
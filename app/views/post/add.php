<?php
    include_once APPROOT.'/views/partials/header.php';
    include_once APPROOT.'/views/partials/navbar.php';
?>

<a class="btn btn-warning pull-right" href="" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<div class="card card-body bg-light mt-5">
    <h2>Crear publicación</h2>
    <p>Por favor introduzca los datos de su publicación</p>
    <form method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title">Título: <sup>*</sup></label>
            <input type="text" name="title" class="form-control <?= !empty($data['title_err'])? 'is-invalid' : ''?>" placeholder="Título de la publicación" value="">
            <span class="invalid-feedback"><?=$data['title_err']?></span>
        </div>

        <div class="form-group">
            <label for="image">Imagen: </label>
            <input type="file" name="image" class="form-control <?= !empty($data['image_err'])? 'is-invalid' : ''?>">
            <span class="invalid-feedback"><?=$data['image_err']?></span>
        </div>

        <div class="form-group">
            <label for="body">Contenido: <sup>*</sup></label>
            <textarea name="body" class="form-control <?= !empty($data['body_err'])? 'is-invalid' : ''?>" rows="5" placeholder="Su contenido">
                
            </textarea>

            <span class="invalid-feedback"><?=$data['body_err']?></span>
        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="Crear publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>

<?php
 include_once APPROOT.'/views/partials/footer.php';
?>
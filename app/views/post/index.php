<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>
<div class="row mb-3">
    <div class="col-md-6">
        <h1>Publicaciones</h1>
    </div>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="<?= URLROOT ?>/posts/add" role="button">
            <i class="fas fa-pencil-alt"></i> Crear publicación
        </a>
    </div>
</div>
<div class="flashes">
    <?= (string) flash() ?>
</div>

<?php
foreach ($data['post'] as $valor) {

?>

    <div class="card">
    <img class="card-img-top" src="<?= URLROOT.'/public/img/'. $valor->image ?>" alt="Card image cap" style="width: 200px;">
        <div class="card-header">
            <?= $valor->title ?>
        </div>
        <div class="card-body">
            <h5 class="card-title"><?= $valor->name ?></h5>
            <p class="card-text"><?= $valor->body ?></p>
            <br>
            <p class="card-text"><?= $valor->created_at ?></p>
            <a href="<?= URLROOT . '/posts/show/' . $valor->postId ?>" class="btn btn-primary">Mas</a>
        </div>
    </div>
<?php } ?>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>
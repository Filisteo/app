<?php 
require APPROOT . '/views/partials/header.php'; 
include_once APPROOT . '/views/partials/navbar.php';
?>
<a class="btn btn-warning pull-right" href="<?= URLROOT?>/posts/index" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<br>
<div class="row mb-3">
    <div class="col-md-12">
    <img class="card-img-top" src="<?=  URLROOT.'/public/img/'. $data['post']->image ?>" alt="Card image cap" style="width: 200px;">
        <h1><?=$data['post']->title ?></h1>

        <div class="bg-secondary text-white p-2 mb-3">
            Creado por: <?= $data['user']->name ?>
        </div>
        <p>
        <?= $data['post']->body ?>
        </p>

        <hr>

        <?php $result = isLoggedh();
            if($result && $data['user']->id = $_SESSION['id']){    
        ?>
        <div class="row">
            <div class="col">
                <a href="<?= URLROOT?>/posts/edit/<?= $data['post']->id?>" class="btn btn-success btn-block">
                    <i class="fas fa-edit"></i> editar
                </a>
            </div>
            <div class="col">
                <form method="POST" action="<?= URLROOT?>/posts/delete/<?= $data['post']->id?>" >
                    <button type="submit" class="btn btn-danger btn-block">
                        <i class="fas fa-trash"></i> Borrar post
                    </button>
                </form>
            </div>
        </div>
        <?php } ?>

    </div>
</div>
<?php require APPROOT . '/views/partials/footer.php'; ?>

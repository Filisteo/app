<?php
    include_once APPROOT.'/views/partials/header.php';
    include_once APPROOT.'/views/partials/navbar.php';
?>

<a class="btn btn-warning pull-right" href="" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<div class="card card-body bg-light mt-5">
    <h2>Edición publicación</h2>
    <p>Por favor introduzca los datos de su publicación</p>
    <form method="POST" action="<?= URLROOT?>/posts/edit/<?= $data['post']->id?>">
        <div class="form-group">
            <label for="title">Título: <sup>*</sup></label>
            <input type="text" name="title" class="form-control <?= !empty($data['title_err'])? 'is-invalid' : ''?>" value="<?= $data['post']->title?>">
            <span class="invalid-feedback"><?=$data['title_err']?></span>
        </div>
        <div class="form-group">
            <label for="body">Contenido: <sup>*</sup></label>
            <textarea name="body" class="form-control <?= !empty($data['body_err'])? 'is-invalid' : ''?>" rows="5">
            <?= $data['post']->body?>
            </textarea>

            <span class="invalid-feedback"><?=$data['body_err']?></span>
        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="Actualizar publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>

<?php
 include_once APPROOT.'/views/partials/footer.php';
?>
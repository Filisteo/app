<?php
if( !session_id() ) @session_start();
use \Dotenv\Dotenv;
require_once 'vendor/autoload.php';
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();
//Como su nombre indica, el autoloader está destinado a cargar de forma automática las clases utilizadas.
include_once 'helpers/utils.php';  
include_once 'helpers/urlHelper.php';    
include_once 'helpers/isLoggedh.php';  
require_once 'exceptions/FileException.php';
require_once 'config/config.php';

spl_autoload_register(function ($className) {
    require_once 'libraries/'.$className.'.php';
});

require_once 'controllers/Paginas.php';
require_once 'controllers/Users.php';
require_once 'controllers/Posts.php';

?>
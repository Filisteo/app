<?php

    class Post {

        private $db;

        public function __construct()
        {
            $this->db = new Database();
        }

        public function getPosts(){
            $this->db->query("SELECT *, posts.id as postId, posts.created_at as postCreatedAt, users.id as userId , users.created_at as userCreatedAt
            FROM posts
            INNER JOIN users
            ON posts.user_id = users.id
            ORDER BY posts.created_at DESC
            ");
                    
            $results = $this->db->resultSet('Post');
            return $results;
        }

        public function addPost($data){
            $this->db->query('insert into posts (title,body,user_id,image) Values (:title,:body,:user_id,:image)');

            $this->db->bind(':title', $data['title']);

            $this->db->bind(':body', $data['body']);
    
            $this->db->bind(':user_id', $data['user_id']);

            $this->db->bind(':image', $data['image']);
    
            $this->db->execute();
        }

        public function getPostByID($id){

            $this->db->query("SELECT * FROM posts WHERE id = :id");
            $this->db->bind(':id', $id);
            $results = $this->db->single('Post');
            return $results;
        }

        public function updatePost($data){
            $this->db->query("UPDATE posts SET title = :title, body = :body WHERE id = :id");

            $this->db->bind(':id', $data['id']);

            $this->db->bind(':title', $data['title']);

            $this->db->bind(':body', $data['body']);

            $this->db->execute();
        }

        public function deletePost($id){
            $this->db->query("DELETE FROM posts WHERE id = :id");
            $this->db->bind(':id', $id);
            $this->db->execute();
        }
    }
?>
<?php

    class User {

        private $db;

        public function __construct()
        {
            $this->db = new Database();
        }

        public function findUserByEmail($email){

            $this->db->query("SELECT * FROM users WHERE email = :email");

            $this->db->bind(':email', $email);

            $results = $this->db->single('User');

            return $results;
        }


        public function register($data){

            $this->db->query("INSERT INTO users (name, email, password) VALUES (:name, :email, :password)");
            
            $this->db->bind(':name', $data['name']);

            $this->db->bind(':email', $data['email']);
    
            $this->db->bind(':password', $data['password']);
    
            
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        public function login($email, $password){

            $this->db->query("SELECT * FROM users WHERE email = :email");

            $this->db->bind(':email', $email);
            $results = $this->db->single('User');
            if(password_verify($password, $results->password)){

                return $results;
            }else{
                return false;
            }             
        }

        public function getUserById($id){
            
            $this->db->query("SELECT * FROM users WHERE id = :id");
            $this->db->bind(':id', $id);
            $results = $this->db->single('User');
            return $results;
        }
    }

?>
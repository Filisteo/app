<?php

class File
{

    private $name;

    private $arrTypes;

    public function __construct($name, $types)
    {
        $this->name = $name;
        $this->arrTypes = $types;
    }

    public function prubeCase()
    {
        if ($this->name['error'] !== UPLOAD_ERR_OK) {
            switch ($this->name['error']) {
                    //  UPLOAD_ERR_INI_SIZE Valor: 1; El fichero subido excede la directiva upload_max_filesize de php.ini.
                    //  upload_max_filesize int El tamaño máximo de un fichero subido. 

                    // Valor: 2; El fichero subido excede la directiva MAX_FILE_SIZE especificada en el formulario HTML.
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    // ini_get — Devuelve el valor de una directiva de configuración
                    throw new FileException('El fichero es demasiado grande. Tamaño máximo: ' . ini_get('upload_max_filesize'));
                    break;
                    // UPLOAD_ERR_PARTIAL Valor: 3; El fichero fue sólo parcialmente subido.
                case UPLOAD_ERR_PARTIAL:
                    throw new FileException("No se ha podido subir el fichero completo");
                    break;
                default:
                    throw new FileException("Error al subir el fichero");
                    break;
            }

        }
    }

    public function saveUploadFile($url)
    {
        // Si la extensión del archivo, no se encuentra en el array de los tipos admitidos, cargar el error
        if (in_array($this->name['type'], $this->arrTypes) === false) {
            throw new FileException("Tipo de fichero no soportado");
            // si el archivo no fue subido mediante HTTP POST
        } else if (is_uploaded_file($this->name['tmp_name']) === false) {
            throw new FileException("El archivo no se ha subido mediante un formulario");
            // Si la imagen no es un archivo válido subido, no sucederá ninguna acción, y move_uploaded_file() devolverá false.
            //Si la imagen es un archivo subido válido, pero no puede ser movido por algunas razones, no sucederá ninguna acción, y move_uploaded_file() devolverá false. Adicionalmente, se emitirá un aviso. 
        } else if (move_uploaded_file($this->name['tmp_name'], $url .$this->name['name']) === false) {
            throw new FileException("Ocurrió algún error al subir el fichero. No pudo guardarse. ");
        }
    }

}

<?php

class Controller {


    public function __construct()
    {
        
    }

    public function model ($modelo) {

        require_once '../app/models/'.$modelo.'.php';
        // Carga el modelo
        $model = new $modelo();

        return $model;
        //Instancia el modelo y lo devuelve
    }

    public function view ($vista, $data=[]) {
       if(file_exists('../app/views/' . $vista . '.php')){
            require_once '../app/views/' . $vista . '.php';
            
        }else{
            echo "No existe la vista";
        }

    }

}

    

?>